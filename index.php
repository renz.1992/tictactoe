<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Tic-Tac-Toe</title>
    <meta name="description" content="Tic-Tac-Toe-Game. Here is a short description for the page. This text is displayed e. g. in search engine result listings." />
    <style>
        table.tic td {
            border: 1px solid #333; /* grey cell borders */
            width: 8rem;
            height: 8rem;
            vertical-align: middle;
            text-align: center;
            font-size: 4rem;
            font-family: Arial;
        }
        table { margin-bottom: 2rem; }
        input.field {
            border: 0;
            background-color: white;
            color: white; /* make the value invisible (white) */
            height: 8rem;
            width: 8rem !important;
            font-family: Arial;
            font-size: 4rem;
            font-weight: normal;
            cursor: pointer;
        }
        input.field:hover {
            border: 0;
            color: #c81657; /* red on hover */
        }
        .colorX { color: #e77; } /* X is light red */
        .colorO { color: #77e; } /* O is light blue */
        table.tic { border-collapse: collapse; }
    </style>
</head>
<body>
    <section>
        <h1>Tic-Tac-Toe</h1>
        <article id="mainContent">
            <h2>Your free browsergame!</h2>
            <p>Type your game instructions here...</p>
            <form method="get" action="index.php">
<?php
//init $board
$board = array(
				array("","",""),
				array("","",""),
				array("","","")
				);

//SESSION for counting turns
if (!isset($_SESSION['turn']))
{
    $_SESSION['turn'] = 0;
}
else
{
    $_SESSION['turn']++;
} 

//switching players after every turn
if($_SESSION['turn'] % 2 === 0)
{
	$player = "X";
}
else
{
	$player = "O";
}

//SESSION for the board
if (isset($_SESSION['board']))
{
	$board = $_SESSION['board'];
}

//destryoing the SESSION
if(isset($_GET["reset"]) ){
    session_destroy();
    header("Location: index.php");
}

//making a turn (actualize the $board)
for ($i = 0; $i < count($board); $i++)
{
	for ($j = 0; $j < count($board[$i]); $j++)
	{
		if(isset($_GET["cell-".$i."-".$j]) )
		{
			$board[$i][$j] = $_GET["cell-".$i."-".$j];
		}
	} 
}

//set the current board to the board SESSION
$_SESSION['board'] = $board;
print_r($board);

echo '<table class="tic">'."\n";
echo '<br />';
//rows
for ($i = 0; $i < count($board); $i++)
{
	echo '<tr>'."\n";
	//columns
	for ($j = 0; $j < count($board[$i]); $j++)
	{
		if ($board[$i][$j] === "") //empty
		{
			echo "\t".'<td><input type="submit" class="field" name="cell-'.$i.'-'.$j.'" value="'.$player.'" /></td>'."\n";
		}
		else //not active
		{
			echo "\t".'<td><span class="color'.$board[$i][$j].'">'.$board[$i][$j].'</span></td>'."\n";
		}
	}
	echo '</tr>'."\n";
}

echo '</table>';

?>
				<!--reset button added-->
				<input type="submit" name="reset" value="Neues Spiel"></input>
            </form>
        </article>
    </section>
</body>
</html>

